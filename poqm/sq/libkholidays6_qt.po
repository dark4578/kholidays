# Albanian translation for kdepim
# Copyright (c) 2008 Rosetta Contributors and Canonical Ltd 2008
# This file is distributed under the same license as the kdepim package.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2008.
#
msgid ""
msgstr ""
"Project-Id-Version: kdepim\n"
"Report-Msgid-Bugs-To: http://bugs.kde.org\n"
"POT-Creation-Date: 2015-06-13 09:34+0000\n"
"PO-Revision-Date: 2009-08-01 20:47+0000\n"
"Last-Translator: Vilson Gjeci <vilsongjeci@gmail.com>\n"
"Language-Team: Albanian <sq@li.org>\n"
"Language: sq\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Launchpad-Export-Date: 2011-04-21 23:33+0000\n"
"X-Generator: Launchpad (build 12883)\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Qt-Contexts: true\n"

#: astroseasons.cpp:139
#, fuzzy
#| msgid "June Solstice"
msgctxt "AstroSeasons|"
msgid "June Solstice"
msgstr "Solstici i Qershorit"

#: astroseasons.cpp:141
#, fuzzy
#| msgid "December Solstice"
msgctxt "AstroSeasons|"
msgid "December Solstice"
msgstr "Solstici i Dhjetorit"

#: astroseasons.cpp:143
#, fuzzy
#| msgid "March Equinox"
msgctxt "AstroSeasons|"
msgid "March Equinox"
msgstr "Ekuinoksi i Marsit"

#: astroseasons.cpp:145
#, fuzzy
#| msgid "September Equinox"
msgctxt "AstroSeasons|"
msgid "September Equinox"
msgstr "Ekuinoksi i Shtatorit"

#: declarative/holidayregionsmodel.cpp:89
msgctxt "HolidayRegionsDeclarativeModel|"
msgid "Region"
msgstr ""

#: declarative/holidayregionsmodel.cpp:91
msgctxt "HolidayRegionsDeclarativeModel|"
msgid "Name"
msgstr ""

#: declarative/holidayregionsmodel.cpp:93
msgctxt "HolidayRegionsDeclarativeModel|"
msgid "Description"
msgstr ""

#: holidayregion.cpp:886
msgctxt "HolidayRegion|Canadian region"
msgid "Quebec"
msgstr ""

#: holidayregion.cpp:889
msgctxt "HolidayRegion|German region"
msgid "Germany, Bavaria"
msgstr ""

#: holidayregion.cpp:891
msgctxt "HolidayRegion|German region"
msgid "Germany, Brandenburg"
msgstr ""

#: holidayregion.cpp:893
msgctxt "HolidayRegion|German region"
msgid "Germany, Berlin"
msgstr ""

#: holidayregion.cpp:895
msgctxt "HolidayRegion|German region"
msgid "Germany, Baden-Wuerttemberg"
msgstr ""

#: holidayregion.cpp:897
msgctxt "HolidayRegion|German region"
msgid "Germany, Hesse"
msgstr ""

#: holidayregion.cpp:899
msgctxt "HolidayRegion|German region"
msgid "Germany, Mecklenburg-Hither Pomerania"
msgstr ""

#: holidayregion.cpp:901
msgctxt "HolidayRegion|German region"
msgid "Germany, Lower Saxony"
msgstr ""

#: holidayregion.cpp:903
msgctxt "HolidayRegion|German region"
msgid "Germany, North Rhine-Westphalia"
msgstr ""

#: holidayregion.cpp:905
msgctxt "HolidayRegion|German region"
msgid "Germany, Rhineland-Palatinate"
msgstr ""

#: holidayregion.cpp:907
msgctxt "HolidayRegion|German region"
msgid "Germany, Schleswig-Holstein"
msgstr ""

#: holidayregion.cpp:909
msgctxt "HolidayRegion|German region"
msgid "Germany, Saarland"
msgstr ""

#: holidayregion.cpp:911
msgctxt "HolidayRegion|German region"
msgid "Germany, Saxony"
msgstr ""

#: holidayregion.cpp:913
msgctxt "HolidayRegion|German region"
msgid "Germany, Saxony-Anhalt"
msgstr ""

#: holidayregion.cpp:915
msgctxt "HolidayRegion|German region"
msgid "Germany, Thuringia"
msgstr ""

#: holidayregion.cpp:918
msgctxt "HolidayRegion|Spanish region"
msgid "Catalonia"
msgstr ""

#: holidayregion.cpp:920
msgctxt "HolidayRegion|UK Region"
msgid "England and Wales"
msgstr ""

#: holidayregion.cpp:922
msgctxt "HolidayRegion|UK Region"
msgid "England"
msgstr ""

#: holidayregion.cpp:924
#, fuzzy
#| msgctxt "zodiac symbol for Libra"
#| msgid "scales"
msgctxt "HolidayRegion|UK Region"
msgid "Wales"
msgstr "peshorja"

#: holidayregion.cpp:926
msgctxt "HolidayRegion|UK Region"
msgid "Scotland"
msgstr ""

#: holidayregion.cpp:928
msgctxt "HolidayRegion|UK Region"
msgid "Northern Ireland"
msgstr ""

#: holidayregion.cpp:930
msgctxt "HolidayRegion|Italian Region"
msgid "South Tyrol"
msgstr ""

#: holidayregion.cpp:932
msgctxt "HolidayRegion|"
msgid "New South Wales"
msgstr ""

#: holidayregion.cpp:934
msgctxt "HolidayRegion|Australian Region"
msgid "Queensland"
msgstr ""

#: holidayregion.cpp:936
msgctxt "HolidayRegion|Australian Region"
msgid "Victoria"
msgstr ""

#: holidayregion.cpp:938
msgctxt "HolidayRegion|Australian Region"
msgid "South Australia"
msgstr ""

#: holidayregion.cpp:940
msgctxt "HolidayRegion|Australian Region"
msgid "Northern Territory"
msgstr ""

#: holidayregion.cpp:942
msgctxt "HolidayRegion|Australian Region"
msgid "Australian Capital Territory"
msgstr ""

#: holidayregion.cpp:944
msgctxt "HolidayRegion|Australian Region"
msgid "Western Australia"
msgstr ""

#: holidayregion.cpp:946
msgctxt "HolidayRegion|Australian Region"
msgid "Tasmania"
msgstr ""

#: holidayregion.cpp:948
msgctxt "HolidayRegion|Bosnian and Herzegovinian Region"
msgid "Republic of Srpska"
msgstr ""

#: holidayregion.cpp:966
msgctxt "HolidayRegion|Holiday type"
msgid "Public"
msgstr ""

#: holidayregion.cpp:968
msgctxt "HolidayRegion|Holiday type"
msgid "Civil"
msgstr ""

#: holidayregion.cpp:970
msgctxt "HolidayRegion|Holiday type"
msgid "Religious"
msgstr ""

#: holidayregion.cpp:972
msgctxt "HolidayRegion|Holiday type"
msgid "Government"
msgstr ""

#: holidayregion.cpp:974
msgctxt "HolidayRegion|Holiday type"
msgid "Financial"
msgstr ""

#: holidayregion.cpp:976
msgctxt "HolidayRegion|Holiday type"
msgid "Cultural"
msgstr ""

#: holidayregion.cpp:978
msgctxt "HolidayRegion|Holiday type"
msgid "Commemorative"
msgstr ""

#: holidayregion.cpp:980
msgctxt "HolidayRegion|Holiday type"
msgid "Historical"
msgstr ""

#: holidayregion.cpp:982
msgctxt "HolidayRegion|Holiday type"
msgid "School"
msgstr ""

#: holidayregion.cpp:984
msgctxt "HolidayRegion|Holiday type"
msgid "Seasonal"
msgstr ""

#: holidayregion.cpp:986
msgctxt "HolidayRegion|Holiday type"
msgid "Name Days"
msgstr ""

#: holidayregion.cpp:988
msgctxt "HolidayRegion|Holiday type"
msgid "Personal"
msgstr ""

#: holidayregion.cpp:990
msgctxt "HolidayRegion|Holiday type"
msgid "Christian"
msgstr ""

#: holidayregion.cpp:992
msgctxt "HolidayRegion|Holiday type"
msgid "Anglican"
msgstr ""

#: holidayregion.cpp:994
msgctxt "HolidayRegion|Holiday type"
msgid "Catholic"
msgstr ""

#: holidayregion.cpp:996
msgctxt "HolidayRegion|Holiday type"
msgid "Protestant"
msgstr ""

#: holidayregion.cpp:998
msgctxt "HolidayRegion|Holiday type"
msgid "Orthodox"
msgstr ""

#: holidayregion.cpp:1000
msgctxt "HolidayRegion|Holiday type"
msgid "Jewish"
msgstr ""

#: holidayregion.cpp:1002
msgctxt "HolidayRegion|Holiday type"
msgid "Jewish Orthodox"
msgstr ""

#: holidayregion.cpp:1004
msgctxt "HolidayRegion|Holiday type"
msgid "Jewish Conservative"
msgstr ""

#: holidayregion.cpp:1006
msgctxt "HolidayRegion|Holiday type"
msgid "Jewish Reform"
msgstr ""

#: holidayregion.cpp:1008
msgctxt "HolidayRegion|Holiday type"
msgid "Islamic"
msgstr ""

#: holidayregion.cpp:1010
msgctxt "HolidayRegion|Holiday type"
msgid "Islamic Sunni"
msgstr ""

#: holidayregion.cpp:1012
msgctxt "HolidayRegion|Holiday type"
msgid "Islamic Shia"
msgstr ""

#: holidayregion.cpp:1014
msgctxt "HolidayRegion|Holiday type"
msgid "Islamic Sufi"
msgstr ""

#: holidayregion.cpp:1020
#, qt-format
msgctxt ""
"HolidayRegion|Holiday file display name, %1 = region name, %2 = holiday type"
msgid "%1 - %2"
msgstr ""

#: holidayregion.cpp:1028
msgctxt "HolidayRegion|Unknown holiday region"
msgid "Unknown"
msgstr ""

#: lunarphase.cpp:25
#, fuzzy
#| msgid "New Moon"
msgctxt "LunarPhase|"
msgid "New Moon"
msgstr "Hëna e re"

#: lunarphase.cpp:27
#, fuzzy
#| msgid "Full Moon"
msgctxt "LunarPhase|"
msgid "Full Moon"
msgstr "Hëna e plotë"

#: lunarphase.cpp:29
#, fuzzy
#| msgid "First Quarter Moon"
msgctxt "LunarPhase|"
msgid "First Quarter Moon"
msgstr "Qereku i parë i hënës"

#: lunarphase.cpp:31
#, fuzzy
#| msgid "Last Quarter Moon"
msgctxt "LunarPhase|"
msgid "Last Quarter Moon"
msgstr "Qereku i fundit i hënës"

#: lunarphase.cpp:35
msgctxt "LunarPhase|"
msgid "Waxing Crescent"
msgstr ""

#: lunarphase.cpp:37
msgctxt "LunarPhase|"
msgid "Waxing Gibbous"
msgstr ""

#: lunarphase.cpp:39
msgctxt "LunarPhase|"
msgid "Waning Gibbous"
msgstr ""

#: lunarphase.cpp:41
msgctxt "LunarPhase|"
msgid "Waning Crescent"
msgstr ""

#: zodiac.cpp:66
#, fuzzy
#| msgid "Aries"
msgctxt "Zodiac|"
msgid "Aries"
msgstr "Dashi"

#: zodiac.cpp:68
#, fuzzy
#| msgid "Taurus"
msgctxt "Zodiac|"
msgid "Taurus"
msgstr "Demi"

#: zodiac.cpp:70
#, fuzzy
#| msgid "Gemini"
msgctxt "Zodiac|"
msgid "Gemini"
msgstr "Binjakët"

#: zodiac.cpp:72
#, fuzzy
#| msgid "Cancer"
msgctxt "Zodiac|"
msgid "Cancer"
msgstr "Gaforrja"

#: zodiac.cpp:74
#, fuzzy
#| msgid "Leo"
msgctxt "Zodiac|"
msgid "Leo"
msgstr "Luani"

#: zodiac.cpp:76
#, fuzzy
#| msgid "Virgo"
msgctxt "Zodiac|"
msgid "Virgo"
msgstr "Virgjëresha"

#: zodiac.cpp:78
#, fuzzy
#| msgid "Libra"
msgctxt "Zodiac|"
msgid "Libra"
msgstr "Peshorja"

#: zodiac.cpp:80
#, fuzzy
#| msgid "Scorpio"
msgctxt "Zodiac|"
msgid "Scorpio"
msgstr "Akrepi"

#: zodiac.cpp:82
#, fuzzy
#| msgid "Sagittarius"
msgctxt "Zodiac|"
msgid "Sagittarius"
msgstr "Shigjetari"

#: zodiac.cpp:84
#, fuzzy
#| msgid "Capricorn"
msgctxt "Zodiac|"
msgid "Capricorn"
msgstr "Bricjapi"

#: zodiac.cpp:86
#, fuzzy
#| msgid "Aquarius"
msgctxt "Zodiac|"
msgid "Aquarius"
msgstr "Ujori"

#: zodiac.cpp:88
#, fuzzy
#| msgid "Pisces"
msgctxt "Zodiac|"
msgid "Pisces"
msgstr "Peshqit"

#: zodiac.cpp:258
#, fuzzy
#| msgctxt "zodiac symbol for Aries"
#| msgid "ram"
msgctxt "HolidayRegion|zodiac symbol for Aries"
msgid "ram"
msgstr "dashi"

#: zodiac.cpp:260
#, fuzzy
#| msgctxt "zodiac symbol for Taurus"
#| msgid "bull"
msgctxt "HolidayRegion|zodiac symbol for Taurus"
msgid "bull"
msgstr "demi"

#: zodiac.cpp:262
#, fuzzy
#| msgctxt "zodiac symbol for Gemini"
#| msgid "twins"
msgctxt "HolidayRegion|zodiac symbol for Gemini"
msgid "twins"
msgstr "binjakët"

#: zodiac.cpp:264
#, fuzzy
#| msgctxt "zodiac symbol for Cancer"
#| msgid "crab"
msgctxt "HolidayRegion|zodiac symbol for Cancer"
msgid "crab"
msgstr "gaforrja"

#: zodiac.cpp:266
#, fuzzy
#| msgctxt "zodiac symbol for Leo"
#| msgid "lion"
msgctxt "HolidayRegion|zodiac symbol for Leo"
msgid "lion"
msgstr "luani"

#: zodiac.cpp:268
#, fuzzy
#| msgctxt "zodiac symbol for Virgo"
#| msgid "virgin"
msgctxt "HolidayRegion|zodiac symbol for Virgo"
msgid "virgin"
msgstr "virgjëresha"

#: zodiac.cpp:270
#, fuzzy
#| msgctxt "zodiac symbol for Libra"
#| msgid "scales"
msgctxt "HolidayRegion|zodiac symbol for Libra"
msgid "scales"
msgstr "peshorja"

#: zodiac.cpp:272
#, fuzzy
#| msgctxt "zodiac symbol for Scorpion"
#| msgid "scorpion"
msgctxt "HolidayRegion|zodiac symbol for Scorpion"
msgid "scorpion"
msgstr "akrepi"

#: zodiac.cpp:274
#, fuzzy
#| msgctxt "zodiac symbol for Sagittarius"
#| msgid "archer"
msgctxt "HolidayRegion|zodiac symbol for Sagittarius"
msgid "archer"
msgstr "shigjetari"

#: zodiac.cpp:276
#, fuzzy
#| msgctxt "zodiac symbol for Capricorn"
#| msgid "goat"
msgctxt "HolidayRegion|zodiac symbol for Capricorn"
msgid "goat"
msgstr "bricjapi"

#: zodiac.cpp:278
msgctxt "HolidayRegion|zodiac symbol for Aquarius"
msgid "water carrier"
msgstr ""

#: zodiac.cpp:280
#, fuzzy
#| msgctxt "zodiac symbol for Pices"
#| msgid "fish"
msgctxt "HolidayRegion|zodiac symbol for Pices"
msgid "fish"
msgstr "peshqit"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr " ,Launchpad Contributions:"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr ","
